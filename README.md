# Astro Starter Kit: Basics

1. install kid core
```sh
npm i @extension-kid/core @dnd-kit/sortable
```

2. install tailwind css
```sh
  npx astro add tailwind   
```
3. install react
```sh
  npx astro add react
```
4. Heroic Icons
```sh
  https://heroicons.com/
```