import { Icons } from "@/icons/Icons";
import type { Column, Id } from "@/utils/types";

interface Props {
  column: Column;
  deleteColumn: (id: Id) => void;
}

export function ColumnContainer(props: Props) {
  const { column, deleteColumn } = props;
  return (
    <div className="bg-columnBackgroundColor opacity-40 border-2 border-pink-500 w-[350px] h-[500px] max-h-[500px] rounded-md flex flex-col">
      {/* --- --- -- Colum title */}
      <div className="bg-mainBackground text-md h-[60px] cursor-grab rounded-md rounded-b-none p-3 font-bold border-columnBackgroundColor border-4 flex items-center justify-between">
        <div className="flex gap-2">
          <div className="flex justify-center items-center bg-columnBackgroundColor px-2 py-1 text-sm rounded-full">
            0
          </div>
          {column.title}
        </div>
        {/* -- --- Delete */}
        <button
          onClick={() => {
            deleteColumn(column.id);
          }}
          className="stroke-gray-500 hover:stroke-white hover:bg-columnBackgroundColor rounded px-1 py-2 "
        >
          <Icons.Delete />
        </button>
      </div>
      {/* --- --- -- Colum task container */}
      <div className="flex flex-grow">Content</div>
      {/* --- --- -- footer */}
      <div>Footer</div>
    </div>
  );
}
