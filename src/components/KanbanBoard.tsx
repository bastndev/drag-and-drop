import { Icons } from "@/icons/Icons";
import "../styles/global.css";
import { useState } from "react";
import type { Column, Id } from "@/utils/types";
import { ColumnContainer } from "./ColumnContainer";

export function KanbanBoard() {
  const [columns, setColumns] = useState<Column[]>([]);
  // console.log(columns);

  return (
    <div className="m-auto flex min-h-screen w-full items-centers overflow-x-auto overflow-y-hidden px-[40px]">
      <div className="m-auto flex gap-4">
        <div className="flex gap-4">
          {columns.map((col) => (
            <ColumnContainer column={col} deleteColumn={deleteColumn} />
          ))}
        </div>
        <button
          onClick={() => {
            createNewColumn();
          }}
          className="h-[60px] w-[350px] min-w-[350px] cursor-pointer rounded-lg bg-mainBackground border-2 border-columnBackgroundColor p-4 ring-rose-500 hover:ring-2 flex gap-2"
        >
          <Icons.Plus />
          add Colum
        </button>
      </div>
    </div>
  );

  // TODO: Implement the following functions
  function createNewColumn() {
    const columnToAdd: Column = {
      id: generateId(),
      title: `Column ${columns.length + 1}`,
    };

    setColumns([...columns, columnToAdd]);
  }

  function deleteColumn(id: Id) {
    const filterColumns = columns.filter((col) => col.id !== id);
    setColumns(filterColumns);
  }
}

function generateId() {
  return Math.floor(Math.random() * 10000);
}
