/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        mainBackground: "#0d1117",
        columnBackgroundColor: "#151c22",
      },
    },
  },
  plugins: [],
};
